//
//  Login.swift
//  SnapGames
//
//  Created by Jaime García Castán on 7/11/17.
//  Copyright © 2017 Jaime García Castán & Ivan Galvez Rochel. All rights reserved.
//

import UIKit
import Firebase

class Login: UIViewController, FirebaseAdminDelegate {
    
    @IBOutlet var tfUser: UITextField?
    @IBOutlet var tfPass: UITextField?
    @IBOutlet var btnSignIn: UIButton?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        FirebaseAdmin.getSharedInstance().setDelegate(delegate: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func signIn() {
        let username = tfUser?.text
        let password = tfPass?.text
        if(username != nil || password != nil) {
            FirebaseAdmin.getSharedInstance().iniciarSesion(email: username!, password: password!)
        }
        
    }
    
    func dataLoaded() {
        self.performSegue(withIdentifier: "LoginTransition", sender: self)
    }
}

