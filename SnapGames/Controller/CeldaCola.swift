//
//  CeldaCola.swift
//  SnapGames
//
//  Created by Jaime García Castán on 14/11/17.
//  Copyright © 2017 Jaime García Castán & Ivan Galvez Rochel. All rights reserved.
//

import UIKit

class CeldaCola: UITableViewCell {
    
    @IBOutlet var img: UIImageView?
    @IBOutlet var lblFechaH: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }

}
