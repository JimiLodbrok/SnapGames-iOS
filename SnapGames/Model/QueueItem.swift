//
//  QueueItem.swift
//  SnapGames
//
//  Created by Jaime García Castán on 20/11/17.
//  Copyright © 2017 Jaime García Castán & Ivan Galvez Rochel. All rights reserved.
//

import UIKit

class QueueItem: NSObject {
    let imgName: String?
    let fecha: String?
    var img: UIImage?
    
    init(imgName: String, fecha: String) {
        self.imgName = imgName
        self.fecha = fecha
    }
}
